using System;
using System.Data;
using Npgsql;

namespace Core.Persistence.Configuration
{
    public interface IDatabaseConnectionString
    {
        string ConnectionString { get; }
    }
    
    public class DatabaseConnectionStringProvider : IDatabaseConnectionString
    {
        private static string _connectionString;

        public string ConnectionString => GetConnectionString();
        
        public static string GetConnectionString()
        {
            if (_connectionString != null) return _connectionString;

            _connectionString = GetConnectionString(GetServer(), GetPort(), GetSchema(), GetUser(), GetPassword());
            return _connectionString;
        }
        
        private static string GetConnectionString(string server, string port, string schema, string user,
            string password) => $"Server={server};Port={port};Database={schema};User Id={user};Password={password};";

        private static string GetServer() => Environment.GetEnvironmentVariable("DB_SERVER") ?? DefaultServer;
        private static string GetPort() => Environment.GetEnvironmentVariable("DB_PORT") ?? DefaultPort;
        private static string GetSchema() => Environment.GetEnvironmentVariable("DB_SCHEMA") ?? DefaultSchema;
        private static string GetUser() => Environment.GetEnvironmentVariable("DB_USER") ?? DefaultUser;
        private static string GetPassword() => Environment.GetEnvironmentVariable("DB_PASSWORD") ?? DefaultPassword;

        private static string DefaultServer => "127.0.0.1";
        private static string DefaultPort => "5432";
        private static string DefaultSchema => "postgres";
        private static string DefaultUser => "postgres";
        private static string DefaultPassword => "admin";
    }
    
    public class DatabaseConnection : IDisposable
    {
        private NpgsqlConnection _con;
        public NpgsqlConnection Db
        {
            get
            {
                if (CanOpen)
                {
                    _con.Open();
                }

                return _con;
            }
        }

        private bool CanOpen => _con.FullState == ConnectionState.Closed;

        public DatabaseConnection(IDatabaseConnectionString connectionString)
        {
            _con = new NpgsqlConnection(connectionString.ConnectionString);
        }

        public void Dispose()
        {
            if (!CanOpen)
            {
                _con.Close();
            }

            _con.Dispose();
            _con = null;
        }
    }
}