using System;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Lifetime;
using Core;
using Core.Domain;
using Core.Persistence.Repositories;
using Core.Services;

namespace Testing.Integration.Config
{
    public class IntegrationTest : IDisposable
    {
        private readonly TestDatabaseInitialiser _db;
        private static IContainer Container { get; set; }

        public IntegrationTest()
        {
            InitialiseContainer();
            Console.WriteLine("Setting up database");
            _db = new TestDatabaseInitialiser(Container);
        }
        
        private static void InitialiseContainer()
        {
            if (Container != null) return;
            var b = new ContainerBuilder();
            DIConfig.Configure(new TestConfig(), b);
            Container = b.Build();
        }

        protected ILifetimeScope GetContainer()
        {
            return Container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag,
                b => { b.Register(c => UserIdentity.NoUser).As<IUserIdentity>(); });
        }

        protected ILifetimeScope GetContainerWithUser(IUserIdentity user)
        {
            return Container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag,
                b => { b.Register(c => user).As<IUserIdentity>(); });
        }

        protected async Task<UserScope> GetContainerWithUser(string email)
        {
            using (var con = _db.GetConnection())
            {
                var userRepo = new UserRepository(con);
                var user = await userRepo.FindByEmail(email);
                var identity = new UserIdentity(user.Id, user.Email, user.Name, user.Role, true);
                var scope = Container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag, b =>
                {
                    b.Register(c => identity).As<IUserIdentity>();
                });

                return new UserScope
                {
                    User = identity,
                    Scope = scope
                };
            }
        }

        public class UserScope : IDisposable
        {
            public ILifetimeScope Scope;
            public IUserIdentity User;

            public void Dispose()
            {
                Scope?.Dispose();
                Scope = null;
            }
        }
        
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}