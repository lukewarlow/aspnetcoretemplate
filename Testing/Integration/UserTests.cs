using System.Threading.Tasks;
using Autofac;
using Core.Services;
using FluentAssertions;
using Testing.Integration.Config;
using Xunit;

namespace Testing.Integration
{
    [Collection("Server")]
    public class UserTests : IntegrationTest
    {
        [Fact]
        public async Task CanFindById()
        {
            using (var container = await GetContainerWithUser("admin@example.com"))
            {
                var service = container.Scope.Resolve<UserService>();
                var exampleEntity = await service.FindById(1);
                exampleEntity.Name.Should().Be("Admin User");
            }
        }
    }
}